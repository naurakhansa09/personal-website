from django import forms

class Schedule(forms.Form):
    activity = forms.CharField(
        label = "Activity", 
        max_length = 30

    )
    date = forms.IntegerField(
        label ="Date",
    )
    isianbulan = (
        ('January','January'),
        ('Februariy','February'),
        ('March','March'),
        ('April','April'),
        ('May','May'),
        ('June','June'),
        ('July','July'),
        ('August','August'),
        ('September','September'),
        ('October','October'),
        ('November','November'),
        ('December','December'),
        
    )
    month = forms.ChoiceField(
        label = "Month",
        choices = isianbulan,

    )
    isiantahun = (
        ('2019','2019'),
        ('2020','2020'),
    )

    year = forms.ChoiceField(
        label = " Year",
        choices = isiantahun, 
    )

    location=forms.CharField(
        label ="Location", 
        max_length = 30
    )
    category=forms.CharField(
        label= "Category", 
        max_length = 30
    )

class Deletejadwal(forms.Form):
    namadelete= forms.CharField(
        label = "Delete Activity", 
    )

