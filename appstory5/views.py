from django.shortcuts import render, redirect
from django.http import HttpResponse


from .forms import Schedule, Deletejadwal
from .models import Schedulemodels

def lihatjadwal(request):
    all= Schedulemodels.objects.all()

    arg= {
        "previous": all
    }
    return render(request, "activities.html",arg)

#def schedule(request):
    #schedules = Schedule.objects.all().order_by('date')
    #return render(request, 'hasil.html', {'schedules': schedules})

#def schedule_create(request):
    #if request.method == 'POST':
       # form = forms.ScheduleForm(request.POST)
        #if form.is_valid():
            #form.save()
            #return redirect('schedule')

    #else:
        #form = forms.ScheduleForm()
    #return render(request, 'jadwalku.html', {'form': form})

def addjadwal(request):
    isianform= Schedule()
    arg={
        "form" : isianform
    }
    return render(request, 'form.html',arg)

def deletejadwal(request):
    isianform= Deletejadwal()
    arg={
        "form" : isianform
    }
    return render(request, 'delete.html',arg)

def jadwal_delete(request):
    getdelete= request.POST["namadelete"]
    all= Schedulemodels.objects.all()

    for e in all: 
        if e.activity == getdelete: 
            e.delete()

    all= Schedulemodels.objects.all()

    arg= {
        "previous": all
    }
    return render(request, "activities.html",arg)


def jadwal_tambah(request):
    getactivity = request.POST["activity"]
    getdate = request.POST["date"]
    getmonth= request.POST["month"]
    getyear = request.POST["year"]
    getlocation = request.POST["location"]
    getcategory= request.POST["category"]

    temp = Schedulemodels(
        activity = getactivity, 
        date = getdate, 
        month= getmonth, 
        year= getyear, 
        location= getlocation ,
        category = getcategory, 
    )

    temp.save()

    all= Schedulemodels.objects.all()

    arg= {
        "previous": all
    }
    return render(request, "activities.html",arg)




