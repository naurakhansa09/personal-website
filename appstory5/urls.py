
from django.urls import path
from . import views
from .views import addjadwal, jadwal_tambah, deletejadwal, jadwal_delete, lihatjadwal
from olaprofile import views


urlpatterns = [
    #path('schedule/',schedule, name='schedule'),
    #path('schedule/create', schedule_create, name='schedule_create'),

    path('index/', views.index, name="home"),
    path('profile/', views.profile, name="profile"),
    path('experience/', views.experience, name="experience"),
    path('contact/', views.contact, name="contact"),

    path('addjadwal/', addjadwal, name= "addjadwal"),
    path('deletejadwal/', deletejadwal, name= "deletejadwal"),

    path('jadwal_tambah/', jadwal_tambah),
    path('jadwal_delete/', jadwal_delete),

    path('lihat_jadwal/', lihatjadwal)
]

     
