from django.urls import path
from . import views


urlpatterns = [
    path('index/', views.index, name = "home"), 
    path('profile/', views.profile, name = "profile"), 
    path('experience/', views.experience, name = "experience"), 
    path('contact/', views.contact, name = "contact"), 
]