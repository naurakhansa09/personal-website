from django.apps import AppConfig


class OlaprofileConfig(AppConfig):
    name = 'olaprofile'
